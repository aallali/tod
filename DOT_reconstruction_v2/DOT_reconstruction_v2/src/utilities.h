#ifndef __UTILITIES_H_INCLUDED__   // if utilities.h hasn't been included yet
#define __UTILITIES_H_INCLUDED__   // #define this so the compiler knows it has been included.

#include <string>
#include <ctime>
#include <iostream>

inline std::string getCurrentTime()
{
	char s[9];
	time_t t = time(NULL);
	struct tm * p = localtime(&t);
	strftime(s, 9, "%H %M %S", p);
	return std::string(s, 9);
}

inline void logInfo(const std::string msg)
{
	std::cout << getCurrentTime() << " | info : " << msg << std::endl;
}

inline void logErr(const std::string msg)
{
	std::cout << getCurrentTime() << " | error : " << msg << std::endl;
}

#endif 
