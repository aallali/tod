/**
 * \file main.cpp
 * \brief Programm to reconstruct optical parameters in time domain
 * \author Anthony Allali
 * \version 2.0
 * \date 14/04/2018
 *
 * Program for solving the DE in time-domain with finite difference and blockingOff or reconstruct images based on the previous model.
 * More details are available in my article "Numerical modeling of time-domain light propagation in 3D turbid media
 * with curved boundaries using finite differences and blocking-off" or "Model-Based Image Reconstruction for Time-Domain Diffuse Optical
*  Tomography with Measurements Normalization"
 *
 */

int main(int argc, char *argv[])
/*!
*  \brief Main function
*
*  Main function of the program that allow to launch reconstruction or diffusion equation solver
*
*  \param argv[1] : Mode of the program ("SIMULATION" or "RECONSTRUCTION"), "SIMULATION" simulate light propagation and save results at path given by argv[6], 
*  "RECONSTRUCTION" load sensors measurements saved in argv[6] and save reconstruction images in argv[7].
*  \param argv[2] : Path of the file which contains all the parameters for the solver except the boundary, absorption and diffusion map. 
*  \param argv[3] : Path of the file which contains the medium map in vector form, if the value is 0, its outside, if 1 its boundary and if 2 its outside. The correspondance between the 3D map and the 1D vector is given by 
* bMap3D[i][j][k]=bMap1D[i+j*nbNodesXaxis+k*nbNodesXaxis*nbNodesZaxis].
*  \param argv[4] : Path of the file which contains the mua map in vector form in cm-1 for the medium to simulate or the initial guess. The correspondance between the 3D map and the 1D vector is given by muaMap3D[i][j][k]=muaMap1D[i+j*nbNodesXaxis+k*nbNodesXaxis*nbNodesZaxis].
*  \param argv[5] : Path of the file which contains the D map in vector form in cm for the medium to simulate or the initial guess. The correspondance between the 3D map and the 1D vector is given by dMap3D[i][j][k]=dMap1D[i+j*nbNodesXaxis+k*nbNodesXaxis*nbNodesZaxis].
*  \param argv[6] : Path of the folder where detectors measurements are saved.
*  \param argv[7] : Path of the folder where reconstruct images will be saved.
*/
{
	return 0;
}